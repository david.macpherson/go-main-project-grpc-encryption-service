module gitlab.com/david.macpherson/go-main-project-grpc-encryption-service

go 1.16

require (
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)
